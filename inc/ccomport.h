#ifndef CCOMPORT_H
#define CCOMPORT_H

#include <QObject>
#include <QSerialPort>
#include <QSerialPortInfo>

#include <./inc/functions.h>

typedef struct
{
  int                   indx;
  quint32               baud;
  QSerialPort::DataBits dbits;
  QSerialPort::StopBits sbits;
  QSerialPort::Parity   parit;
} t_comparam;

class CComPort : public QObject
{
  Q_OBJECT
public:
  explicit CComPort(QObject *parent = nullptr);
  QList<QSerialPortInfo> getAvailablesPorts(void);
  QList<QString>         getInfo(int num);

private:
  QSerialPort        *comport;
  QSerialPortInfo    *cominfo;

private slots:
  void                slotReadyRead(void);

signals:
  void                signStrToLog(QString istr);

public slots:
  void                slotConnect(t_comparam p);
  void                slotDisconnect(void);
  void                slotSent(QByteArray dat);
};

#endif // CCOMPORT_H
