QT += core gui network serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET   = net_tester
TEMPLATE = app

SOURCES   += ./src/main.cpp       \
             ./src/mainwindow.cpp \
             ./src/functions.cpp  \
             ./src/ctcpserver.cpp \
             ./src/ctcpclient.cpp \
             ./src/ccomport.cpp

HEADERS   += ./inc/mainwindow.h  \
             ./inc/functions.h   \
             ./inc/ctcpserver.h  \
             ./inc/ctcpclient.h  \
             ./inc/ccomport.h

FORMS     += mainwindow.ui

RESOURCES += ./res/resources.qrc

RC_FILE   += ./res/res.rc
